// For Firebase JS SDK v7.20.0 and later, measurementId is optional
import firebase from 'firebase'

const firebaseConfig = {
    apiKey: "AIzaSyCIAHPZWdTdZG9rTfuF29GG3IdRlQ24ojU",
    authDomain: "facebook-clone-336f1.firebaseapp.com",
    databaseURL: "https://facebook-clone-336f1.firebaseio.com",
    projectId: "facebook-clone-336f1",
    storageBucket: "facebook-clone-336f1.appspot.com",
    messagingSenderId: "851087726135",
    appId: "1:851087726135:web:38196af869207a427f91b2",
    measurementId: "G-8C4VNWW99V"
  };

  const firebaseApp = firebase.initializeApp(firebaseConfig)
  const db = firebaseApp.firestore()
  const auth = firebase.auth()

  const provider = new firebase.auth.GoogleAuthProvider
  export { auth,provider}
  export default db