import React,{useEffect, useState} from 'react'
import "./Feed.css"
import MessageSender from './MessageSender'
import StoryReel from './StoryReel'
import Post2 from './Post2'
import db from './firebase'
function Feed() {
    const [posts,setPost] = useState([])
    useEffect(()=>{
        db.collection('posts')
        .orderBy('timestamp','desc')
        .onSnapshot(snapShot=>{
            setPost(snapShot.docs.map((doc)=>({id:doc.id,data:doc.data()})))
        })
        console.log(posts);
    },[])

    return (
        <div className="feed">
            <StoryReel />
            <MessageSender />
            {posts.map((post)=>(
                <Post2 
                key={post.data.id}
                profilePic={post.data.profilePic}
                message={post.data.message}
                timestamp={post.data.timestamp}
                username={post.data.username}
                image={post.data.image}
                />
            ))}
        </div>
    )
}

export default Feed
