import React from "react";
import "./Widget.css";
function Widget() {
    return (
        <div className="widget">
            <iframe
                src="https://firebasestorage.googleapis.com/v0/b/facebook-clone-336f1.appspot.com/o/22.png?alt=media&token=01788098-327b-4885-aad0-798a55d5d46c"
                width="250"
                height="100%"
                style={{ border: "non", overflow: "hidden" }}
                scrolling="no"
                frameBorder="0"
                allowTransparency="true"
                allow="encryted-media"
                
            ></iframe>
        </div>
    );
}

export default Widget;
