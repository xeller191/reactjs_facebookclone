import React from 'react'
import './Login.css'
import {auth,provider} from './firebase'
import { useStateValue } from './StateProvider'
import { actionType } from './reducer'
function Login() {
    const [state,dispatch] = useStateValue()
    const signIn= () =>{
        console.log(provider)
        auth.signInWithPopup(provider)
        .then((result) => {
            dispatch({
                type:actionType.SET_USER,
                user:result.user
            })
            console.log(result);
        }).catch((error)=>alert(error))
    }

    return (
        <div className="login">
            <div className="login_logo">
                <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/0/05/Facebook_Logo_%282019%29.png/1024px-Facebook_Logo_%282019%29.png" alt=""/> 
            </div>
            <button type="submit" onClick={signIn}> 
                Sign In
            </button>
        </div>
    )
}

export default Login
